/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx,mdx}",],
  theme: {
    colors: {
      grey: '#3A3A3C',
      lightGrey: '#3f3a3a99',
      yellow: '#BEA11F',
      green: '#538D4E',
      white: '#FFFFFF',
      offWhite: '#FCFCFC',
      black: '#050606',
      blue: '#007AFF'
    },
    extend: {
      backgroundImage: () => ({
        'gradient-grey-to-black': "linear-gradient(180deg, #212226 0%, #000 100%)",
      }),

      gap: {
        game: '10px',
      },
      fontSize: {
        game: '32px'
      }
    },
  },
  plugins: [],
}


import {
  compareGuessAndAnswer,
  CheckedLetterColor,
  CheckGuess,
} from "./compareGuessAndAnswer";

describe("compareGuessAndAnswer function", () => {
  let setGameWon: jest.Mock<any, any, any>;
  let setGameLost: jest.Mock<any, any, any>;

  beforeEach(() => {
    setGameWon = jest.fn();
    setGameLost = jest.fn();
  });

  it("should call setGameWon if the guess is correct", () => {
    const answer = "test";
    const checkGuess: CheckGuess = { guess: "test", attemptNumber: 1 };

    const result = compareGuessAndAnswer(
      answer,
      setGameWon,
      setGameLost
    )(checkGuess);

    expect(setGameWon).toHaveBeenCalled();
    expect(setGameLost).not.toHaveBeenCalled();
    expect(result).toBeUndefined();
  });

  it("should call setGameLost if the attempt number is 4 and guess is incorrect", () => {
    const answer = "test";
    const checkGuess: CheckGuess = { guess: "fail", attemptNumber: 4 };

    const result = compareGuessAndAnswer(
      answer,
      setGameWon,
      setGameLost
    )(checkGuess);

    expect(setGameLost).toHaveBeenCalled();
    expect(setGameWon).not.toHaveBeenCalled();
    expect(result).toBeUndefined();
  });

  it("should return correct color coding for each letter in the guess", () => {
    const answer = "abcd";
    const checkGuess: CheckGuess = { guess: "abef", attemptNumber: 1 };

    const expectedResponse = [
      CheckedLetterColor.GREEN,
      CheckedLetterColor.GREEN,
      CheckedLetterColor.GREY,
      CheckedLetterColor.GREY,
    ];

    const result = compareGuessAndAnswer(
      answer,
      setGameWon,
      setGameLost
    )(checkGuess);

    expect(result).toEqual(expectedResponse);
  });

  it("should only match a letter once", () => {
    const answer = "aaaa";
    const checkGuess: CheckGuess = { guess: "abef", attemptNumber: 1 };

    const expectedResponse = [
      CheckedLetterColor.GREEN,
      CheckedLetterColor.GREY,
      CheckedLetterColor.GREY,
      CheckedLetterColor.GREY,
    ];

    const result = compareGuessAndAnswer(
      answer,
      setGameWon,
      setGameLost
    )(checkGuess);

    expect(result).toEqual(expectedResponse);
  });

  it("should be yellow if right but in wrong position and only match once", () => {
    const answer = "bccc";
    const checkGuess: CheckGuess = { guess: "abeb", attemptNumber: 1 };

    const expectedResponse = [
      CheckedLetterColor.GREY,
      CheckedLetterColor.YELLOW,
      CheckedLetterColor.GREY,
      CheckedLetterColor.GREY,
    ];

    const result = compareGuessAndAnswer(
      answer,
      setGameWon,
      setGameLost
    )(checkGuess);

    expect(result).toEqual(expectedResponse);
  });
});

export enum CheckedLetterColor {
  YELLOW = "yellow",
  GREEN = "green",
  GREY = "grey",
}

export interface CheckGuess {
  guess: string;
  attemptNumber: number;
}

export const compareGuessAndAnswer = (
  answer: string,
  setGameWon: () => void,
  setGameLost: () => void
) => {
  return ({
    guess,
    attemptNumber,
  }: CheckGuess): CheckedLetterColor[] | undefined => {
    // check if the answer is fully correct
    if (guess === answer) {
      setGameWon();
      return;
    }

    // if by attempt 4 it hasn't been guessed, we know the answer is wrong and the game is lost
    if (attemptNumber === 4) {
      setGameLost();
      return;
    }

    const correctAndCorrectPosition: number[] = [];

    // This will store the characters from 'answer' that haven't been matched yet.
    // We replace matched characters with a color to display to the user
    const guessArray: (string | CheckedLetterColor)[] = guess.split("");

    // Check what letters are right and in the right position
    for (let i = 0; i < guess.length; i++) {
      if (guess[i] === answer[i]) {
        correctAndCorrectPosition.push(i);
        // mark the color as green
        guessArray[i] = CheckedLetterColor.GREEN;
      }
    }

    for (let i = 0; i < guess.length; i++) {
      // If this index is already known as correct, we skip it.
      if (correctAndCorrectPosition.includes(i)) {
        continue;
      }

      // check what letters are right but in the wrong position and find their index
      const matchIndex = guessArray.indexOf(answer[i]);

      if (matchIndex !== -1) {
        // mark the color as yellow
        guessArray[matchIndex] = CheckedLetterColor.YELLOW;
      }
    }

    // At this point, any character in guessArray that hasn't been matched should be marked as GREY.
    const resultArray = guessArray.map((item) =>
      [CheckedLetterColor.YELLOW, CheckedLetterColor.GREEN].includes(
        item as CheckedLetterColor
      )
        ? item
        : CheckedLetterColor.GREY
    );

    return resultArray as CheckedLetterColor[];
  };
};

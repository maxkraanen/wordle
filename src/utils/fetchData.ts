export const fetchData = async <T>(
  url: string,
  maxRetries: number = 3
): Promise<T | undefined> => {
  let attempts = 0;

  while (attempts < maxRetries) {
    try {
      const res = await fetch(url, { cache: "no-store" });

      if (!res.ok) {
        throw new Error(`Failed to fetch data on attempt ${attempts + 1}`);
      }

      return (await res.json()) as T;
    } catch (error) {
      attempts++;
      if (attempts === maxRetries) {
        throw new Error("Failed to fetch data after multiple attempts");
      }
    }
  }
};

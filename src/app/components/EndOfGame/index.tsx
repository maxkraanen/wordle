interface Props {
  emoji: string;
  title: string;
  text: string;
}

export const EndOfGame: React.FC<Props> = ({ emoji, title, text }) => {
  const handleReload = () => {
    location.reload();
  };
  return (
    <div
      className="px-4 pt-5 pb-4 w-[260px] flex flex-col
      items-center gap-4 shrink-0 rounded-[10px] bg-lightGrey backdrop-blur-2xl"
    >
      <p className="text-[64px] p-3">{emoji}</p>
      <h1 className="text-offWhite font-semibold text-[13px] leading-4">
        {title}
      </h1>
      <p className="font-normal text-[11px] leading-[14px] text-offWhite text-center">
        {text}
      </p>
      <button
        onClick={handleReload}
        className="py-[6px] w-full flex justify-center bg-blue text-white rounded-[5px]"
      >
        Try again
      </button>
    </div>
  );
};

import React from "react";
import { CheckedLetterColor } from "@/utils";
import { theme } from "../../../../tailwind.config";

interface Props {
  value: string | undefined;
  color?: CheckedLetterColor;
  index: number;
}

export const Cell: React.FC<Props> = ({ value, color, index }) => {
  const [displayColor, setDisplayColor] = React.useState<string | undefined>();

  const styles = [
    "h-[60px]",
    "w-[60px]",
    "rounded",
    "flex",
    "items-center",
    "justify-center",
    // Apply border if displayColor is undefined
    ...(displayColor ? [] : ["border-2", "border-grey"]),
  ].join(" ");

  React.useEffect(() => {
    // calculate delay for showing color
    if (color) {
      const delay = 200 * index;
      const timer = setTimeout(() => {
        setDisplayColor(theme?.colors?.[color as keyof typeof theme.colors]);
      }, delay);

      return () => clearTimeout(timer);
    }
  }, [color, index]);

  return (
    <div className={styles} style={{ backgroundColor: displayColor }}>
      <p className="text-white font-bold text-game leading-10">
        {value?.toUpperCase()}
      </p>
    </div>
  );
};

import React from "react";
import { Cell } from "../Cell";
import { CheckGuess, CheckedLetterColor } from "@/utils";

interface Props {
  checkGuess: (params: CheckGuess) => CheckedLetterColor[] | undefined;
  goToNextRow: () => void;
  isActive: boolean;
  attemptNumber: number;
}

export const Row: React.FC<Props> = ({
  checkGuess,
  goToNextRow,
  isActive,
  attemptNumber,
}) => {
  const [value, setValue] = React.useState("");
  // checkedGuess will hold the colors after we checked the answer
  const [checkedGuess, setCheckedGuess] = React.useState<
    undefined | CheckedLetterColor[]
  >(undefined);

  const handleKeyDown = React.useCallback(
    (event: KeyboardEvent) => {
      // only allow submit when 5 chars are entered. Check answer and if
      // not correct or last attempt we go to next row
      if (event.key == "Enter" && value.length === 5) {
        setCheckedGuess(checkGuess({ guess: value, attemptNumber }));
        goToNextRow();
      }

      if (event.key == "Backspace") {
        // Remove last char on backspace
        setValue(value.slice(0, -1));
      }

      if (/^[a-z]$/i.test(event.key) && value.length < 5) {
        // add char if it's a letter and we don't have 5 yet
        setValue((prev) => prev + event.key);
      }
    },
    [attemptNumber, checkGuess, goToNextRow, value]
  );

  React.useEffect(() => {
    if (isActive) {
      window.addEventListener("keydown", handleKeyDown);
    }

    return () => window.removeEventListener("keydown", handleKeyDown);
  }, [handleKeyDown, isActive]);

  const cells = [0, 1, 2, 3, 4];

  return (
    <>
      <div className="grid grid-cols-5 gap-game w-fit">
        {cells.map((cell, index) => {
          return (
            <Cell
              key={cell}
              value={value[index]}
              index={index}
              // after check we pass the color to display
              // the right background color
              color={checkedGuess?.[index]}
            />
          );
        })}
      </div>
    </>
  );
};

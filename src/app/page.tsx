import { fetchData } from "@/utils";
import { Home } from "./views";

export default async function Page() {
  const WORD_URL = "https://random-word-api.herokuapp.com/word?length=5";
  const wordToGuess = await fetchData<string[]>(WORD_URL);

  return <Home wordToGuess={wordToGuess?.[0]} />;
}

"use client";

import { EndOfGame, Row } from "@/app/components";
import { compareGuessAndAnswer } from "@/utils";
import React from "react";

enum GameState {
  IN_PRROGRESS = "inProgress",
  WON = "won",
  LOST = "lost",
}

interface Props {
  wordToGuess?: string;
}

export const Home: React.FC<Props> = ({ wordToGuess }) => {
  const [gameState, setGameState] = React.useState(GameState.IN_PRROGRESS);
  const [attemptNumber, setAttemptNumber] = React.useState(0);

  if (!wordToGuess) {
    return <div>Something went wrong</div>;
  }

  const setGameWon = () => setGameState(GameState.WON);
  const setGameLost = () => setGameState(GameState.LOST);

  const checkGuess = compareGuessAndAnswer(
    wordToGuess,
    setGameWon,
    setGameLost
  );

  const handleProgress = (index: number) => {
    // go to the next attempt, there's a maximum of 5 attempts
    setAttemptNumber(Math.min(index + 1, 4));
  };

  const activeElement = {
    [GameState.WON]: (
      <EndOfGame
        emoji="🏆"
        title="You're a Winner, Champ!"
        text="Congrats! You've just crushed it and won the game. Now, bask in your glory and celebrate like a boss! 🎉"
      />
    ),
    [GameState.LOST]: (
      <EndOfGame
        emoji="🙈"
        title="Oops! Tough Luck, But Don't Give Up!"
        text="You didn't quite make it this time, but hey, no worries! Give it another shot, and who knows, the next round might be your moment of glory! Keep going, champ! 💪🎮"
      />
    ),
    [GameState.IN_PRROGRESS]: (
      <div className="bg-gradient-grey-to-black flex justify-center w-full min-h-screen">
        <div className="grid gap-game h-fit pt-40">
          {[0, 1, 2, 3, 4].map((attempt, index) => (
            <Row
              checkGuess={checkGuess}
              attemptNumber={attemptNumber}
              key={attempt}
              isActive={attemptNumber === index}
              goToNextRow={() => handleProgress(index)}
            />
          ))}
        </div>
      </div>
    ),
  };

  return (
    <main className="min-h-screen flex justify-center items-center bg-black ">
      {activeElement[gameState]}
    </main>
  );
};
